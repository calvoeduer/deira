﻿using DAL;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BLL
{
   public class NinoService
    {


        private OracleConnection conexion;
       
        private NinoRepository ninoRepository;

        private string cadenaConexion = @"User Id=yudis; password=yudis; Data Source=localhost:1521/xepdb1; Pooling=false;";




        public NinoService()
        {
            conexion = new OracleConnection(cadenaConexion);

            ninoRepository = new NinoRepository(conexion);
        }




        public string Guardar(Nino nino)

        {

            try
            {
                conexion.Open();

                ninoRepository.Guardar(nino);
                return $"se guardaron los datos correctamente de beneficiario niño";
            }
            catch (System.Exception eg)
            {

                return "Error de Datos " + eg.Message;
            }
            finally
            {
                conexion.Close();
            }
        }
    }
}

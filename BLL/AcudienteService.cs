﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using DAL;
using Entity;

namespace BLL
{
   public class AcudienteService
    {
        private OracleConnection conexion;
        private AcudienteRepository acudienteRepository;

        private string cadenaConexion = @"User Id=yudis; password=yudis; Data Source=localhost:1521/xepdb1; Pooling=false;";


        public AcudienteService()
        {
            conexion = new OracleConnection(cadenaConexion);
       
            acudienteRepository = new AcudienteRepository(conexion);
        }


        public string Guardar(Acudiente acudiente)
        {

            try
            {
                conexion.Open();

                acudienteRepository.Guardar(acudiente);

                return $"se guardaron los datos correctamente";
            }
            catch (System.Exception eg)
            {

                return "Error de Datos " + eg.Message;
            }
            finally
            {
                conexion.Close();
            }
        }
    }
}

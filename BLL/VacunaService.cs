﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using DAL;
using Entity;



namespace BLL
{
    public class VacunaService
    {


        private OracleConnection conexion;
        private VacunaRepository vacunaRepository;

        private string cadenaConexion = @"User Id=yudis; password=yudis; Data Source=localhost:1521/xepdb1; Pooling=false;";


        public VacunaService()
        {
            conexion = new OracleConnection(cadenaConexion);
            vacunaRepository = new VacunaRepository(conexion);
        }


        public string Guardar(Vacuna vacuna)
        {

            try
            {
                conexion.Open();

                vacunaRepository.Guardar(vacuna);

                return $"se guardaron los datos correctamente";
            }
            catch (System.Exception eg)
            {

                return "Error de Datos " + eg.Message;
            }
            finally
            {
                conexion.Close();
            }
        }


        public List<Vacuna> Consultar()
        {
            try
            {
                conexion.Open();
                List<Vacuna> vacunas = vacunaRepository.Consultar();
                return vacunas;

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                conexion.Close();
            }

        }


    }
}

﻿using DAL;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BLL
{
    class ImcService
    {
        private OracleConnection conexion;
        
        private ImcRepository imcRepository;
        private string cadenaConexion = @"User Id=yudis; password=yudis; Data Source=localhost:1521/xepdb1; Pooling=false;";

        public ImcService()
        {
            conexion = new OracleConnection(cadenaConexion);
            ImcRepository imcRepository = new ImcRepository(conexion);
        }

        public string Guardar(IMC iMC)
        {

            try
            {
                conexion.Open();

                imcRepository.Guardar(iMC);
            
                return $"se guardaron los datos correctamente";
            }
            catch (System.Exception eg)
            {

                return "Error de Datos " + eg.Message;
            }
            finally
            {
                conexion.Close();
            }
        }



    }
}

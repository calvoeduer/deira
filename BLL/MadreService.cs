﻿using DAL;
using Entity;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class MadreService
    {

        private OracleConnection conexion;

        private MadreRepository madreRepository;

        private string cadenaConexion = @"User Id=yudis; password=yudis; Data Source=localhost:1521/xepdb1; Pooling=false;";




        public MadreService()
        {
            conexion = new OracleConnection(cadenaConexion);

            madreRepository = new MadreRepository(conexion);
        }




        public string Guardar(Madre madre)

        {

            try
            {
                conexion.Open();

                
                madreRepository.Guardar(madre);
                return $"se guardaron los datos correctamente de beneficiario madre";
            }
            catch (System.Exception eg)
            {

                return "Error de Datos " + eg.Message;
            }
            finally
            {
                conexion.Close();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class Beneficiario : Persona
    {
        public int EdadAños { get; set; }

        public byte[] CarnetVacunacion { get; set; }
        public byte[] Fosyga { get; set; }
        public string PuntajeSistema { get; set; }
        public string PuntajeSisben { get; set; }



        public List<IMC> ListImc { get; set; }
        public List<VacunaPuesta> vacunas { get; set; }
        public List<Asistencia> asistencias { get; set; }

       

        public void AgregarIMC(double peso, double altura)
        {
            IMC imc = new IMC();
            imc.Peso = peso;
            imc.Altura = altura;
            imc.Imc = imc.CalcularIMC();

            ListImc.Add(imc);

        }

        /*lisa*/

        public void AgregarVacuna(Vacuna _vacuna, DateTime fecha)
        {
            VacunaPuesta vacuna_name = new VacunaPuesta();
            vacuna_name.Vacuna = _vacuna;
            vacuna_name.Fecha = fecha;


            vacunas.Add(vacuna_name);
        }

        public void AgregarAsistencia()
        {
            Asistencia asistencia = new Asistencia();
            asistencia.FechaAsistencia = DateTime.Now;

            asistencias.Add(asistencia);
        }

        public int CalcularEdadAños()
        {
            DateTime FechaActual = DateTime.Today;

            if (FechaNacimiento > FechaActual)
            {
                Console.WriteLine("La fecha de nacimiento es mayor que la actual");
                return -1;
            }
            else
            {
                EdadAños = FechaActual.Year - FechaNacimiento.Year;
                if (FechaNacimiento.Month > FechaActual.Month)
                {
                    EdadAños = EdadAños - 1;
                }
            }
            return EdadAños;
        }

   

    }
}

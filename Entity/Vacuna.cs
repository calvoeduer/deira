﻿namespace Entity
{
    public class Vacuna
    {
        public string IdVacuna { get; set; }
        public string NombreVacuna { get; set; }
        public int EdadRecomendada { get; set; }

    }
}

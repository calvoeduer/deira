﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class Asistencia 
    {
        Beneficiario beneficiario = new Beneficiario();
        public DateTime FechaAsistencia { get; set; }



        public override string ToString()
        {

            return $" Identificacion: {beneficiario.Identificacion}    Primer Nomnre:{beneficiario.PrimerNombre}" +
                $" Segundo Nombre   {beneficiario.SegundoNombre}    Primer Apellido {beneficiario.PrimerApellido}   Segundo Apellido {beneficiario.SegundoApellido}" +
                $"     Fecha {DateTime.Today} ";
        }
    }
}

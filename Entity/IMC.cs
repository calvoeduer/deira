﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
  public  class IMC
    {
       

        public double Peso { get; set; }
        public double Altura { get; set; }
        public double Imc { get; set; }

        public double CalcularIMC()
        {
            Imc = Peso / Math.Pow(Altura, 2);

            return Imc;
        }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class VacunaPuesta
    {
        public Vacuna Vacuna { get; set; }

        public DateTime Fecha { get; set; }
    }
}

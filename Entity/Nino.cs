﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Nino : Beneficiario
    {
        public Nino()
        {
            
        }

        public byte[] CarnetCrecimeintoDesarrolo { get; set; }
        public List<Acudiente> acudientes { get; set; }

        public void AgregarAcudiente(Acudiente acudiente, string parentesco, string correo)
        {
            acudiente.Parentesco = parentesco;
            acudiente.Correo = correo;
            acudientes.Add(acudiente);
        }

    }
}

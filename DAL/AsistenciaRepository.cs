﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace DAL
{
    class AsistenciaRepository
    {
        private OracleConnection conexion;
        List<Asistencia> asistencias;
        OracleDataReader Reader;

       
        public AsistenciaRepository(OracleConnection conexionDB)
        {
            asistencias = new List<Asistencia>();
            conexion = conexionDB;
        }

        public void Guardar(Vacuna vacuna)
        {
            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "INSERT INTO vacunas(idvacuna, nombrevacuna, edadrecomendada) VALUES(:idvacuna, :nombrevacuna, :edadrecomendada)";
                comando.Parameters.Add(":idvacuna", vacuna.IdVacuna);
                comando.Parameters.Add(":nombrevacuna", vacuna.NombreVacuna);
                comando.Parameters.Add(":edadrecomendada", vacuna.EdadRecomendada);

                comando.ExecuteNonQuery();

            }
        }
    }
}

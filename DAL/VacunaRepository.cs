﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Entity;

namespace DAL
{
   public class VacunaRepository
    {
        private OracleConnection conexion;
        List<Vacuna> vacunas;
        OracleDataReader Reader;

        public VacunaRepository(OracleConnection conexionDB)
        {
            vacunas = new List<Vacuna>();
            conexion = conexionDB;

        } 


        public void Guardar(Vacuna vacuna)
        {
            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "INSERT INTO vacunas(idvacuna, nombrevacuna, edadrecomendada) VALUES(:idvacuna, :nombrevacuna, :edadrecomendada)";
                comando.Parameters.Add(":idvacuna", vacuna.IdVacuna);
                comando.Parameters.Add(":nombrevacuna", vacuna.NombreVacuna);
                comando.Parameters.Add(":edadrecomendada", vacuna.EdadRecomendada);

                comando.ExecuteNonQuery();

            }
        }

        public Vacuna Map(OracleDataReader reader)
        {
            Vacuna vacuna = new Vacuna();

            vacuna.IdVacuna = (string)reader["IDVACUNA"];
            vacuna.NombreVacuna = (string)reader["NOMBREVACUNA"];
            vacuna.EdadRecomendada = (int)reader["EDADRECOMENDADA"];
            return vacuna;
        }

        public List<Vacuna> Consultar()
        {
            vacunas.Clear();

            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "SELECT * FROM vacunas";
                Reader = comando.ExecuteReader();

                while (Reader.Read())
                {
                    Vacuna vacuna = new Vacuna();
                    vacuna = Map(Reader);
                    vacunas.Add(vacuna);

                }
            }

            return vacunas;

        } 

        public void Eliminar(string idVacuna)
        {
            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "DELETE FROM vacunas WHERE IDVACUNA@IDVACUNA";
                comando.Parameters.Add("@IDVACUNA", idVacuna);

                comando.ExecuteNonQuery();
            }
        }

        public void Modificar(Vacuna vacuna)
        {
            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "UPDATE vacunas SET idvacuna=@IDVACUNA, nombreVacuna=@NOMBREVACUNA, edadrecomendada=@EEDADRECOMENDADA";
                comando.Parameters.Add("@IDVACUNA", OracleDbType.Varchar2).Value = vacuna.IdVacuna;
                comando.Parameters.Add("@NOMBREVACUNA", OracleDbType.Varchar2).Value = vacuna.NombreVacuna;
                comando.Parameters.Add("@EDADRECOMENDADA", OracleDbType.Decimal).Value = vacuna.EdadRecomendada;
                comando.ExecuteNonQuery();


            }
        }

        public Vacuna BuscarId(string idVacuna)
        {

            return vacunas.Find(v => v.IdVacuna.Equals(idVacuna));

        }
    }
}

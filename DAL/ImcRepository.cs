﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using Oracle.ManagedDataAccess.Client;

namespace DAL
{
    public class ImcRepository
    {

        private OracleConnection conexion;
        List<IMC> IMCs;
        OracleDataReader Reader;


        public ImcRepository(OracleConnection conexionDB)
        {
            IMCs = new List<IMC>();
            conexion = conexionDB;
        }


        public void Guardar(IMC iMC)
        {
            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "INSERT INTO vacunas IMCS(:PESO,:ALTURA)";
                comando.Parameters.Add(":PESO", OracleDbType.Double).Value = iMC.Peso ;
                comando.Parameters.Add(":ALTURA", OracleDbType.Double).Value = iMC.Altura;
               
                comando.ExecuteNonQuery();

            }
        }

        public IMC Map(OracleDataReader reader)
        {
            IMC iMC = new IMC();

            iMC.Peso = (double)reader["PESO"];
            iMC.Altura = (double)reader["ALTURA"];
            
            return iMC;
        }

        public List<IMC> Consultar()
        {
            IMCs.Clear();

            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "SELECT * FROM IMCs";
                Reader = comando.ExecuteReader();

                while (Reader.Read())
                {
                    IMC iMC = new IMC();
                    iMC = Map(Reader);
                    IMCs.Add(iMC);
                }
            }

            return IMCs;

        }

     
    }
}

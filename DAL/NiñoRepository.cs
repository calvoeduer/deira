﻿using Entity;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class NinoRepository
    {

        private OracleConnection conexion;
        List<Nino> ninos;
        OracleDataReader Reader;

        public NinoRepository(OracleConnection conexionDB)
        {
            ninos = new List<Nino>();
            conexion = conexionDB;

        }


        public void Guardar(Nino nino)
        {
            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "INSERT INTO ninos(NUMERODOCUMENTO, PRIMERNOMBRE, SEGUNDONOMBRE, PRIMERAPELLIDO, SEGUNDOAPELLIDO, FECHANACIMIENTO, SEXO, PUNTAJESISBEN) " +
                    " VALUES(:NUMERODOCUMENTO, :PRIMERNOMBRE, :SEGUNDONOMBRE, :PRIMERAPELLIDO, :SEGUNDOAPELLIDO, :FECHANACIMIENTO, :SEXO, :PUNTAJESISBEN)";

                comando.Parameters.Add(":NUMERODOCUMENTO", nino.Identificacion);
                comando.Parameters.Add(":PRIMERNOMBRE", nino.PrimerNombre);
                comando.Parameters.Add(":SEGUNDONOMBRE", nino.SegundoNombre);
                comando.Parameters.Add(":PRIMERAPELLIDO", nino.PrimerApellido);
                comando.Parameters.Add(":SEGUNDOAPELLIDO", nino.SegundoApellido);
                comando.Parameters.Add(":FECHANACIMIENTO", nino.FechaNacimiento);
                comando.Parameters.Add(":SEXO", nino.Sexo);
                comando.Parameters.Add(":PUNTAJESISBEN", nino.PuntajeSisben);
            
                comando.ExecuteNonQuery();

            }
        }
    }
}

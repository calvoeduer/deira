﻿using Entity;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class AcudienteRepository
    {

        private OracleConnection conexion;
        List<Acudiente> acudientes;
        OracleDataReader Reader;

        public AcudienteRepository(OracleConnection conexionDB)
        {
            acudientes = new List<Acudiente>();
            conexion = conexionDB;

        }


        public void Guardar(Acudiente acudiente)
        {
            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "INSERT INTO acudientes(NUMERODOCUMENTO, NOMBRE, APELLIDO, PARENTESCO, CORREO, SEXO) " +
                                              " VALUES(:NUMERODOCUMENTO, :NOMBRE, :APELLIDO, :PARENTESCO, :CORREO, :SEXO)";
                comando.Parameters.Add(":NUMERODOCUMENTO", acudiente.Identificacion);
                comando.Parameters.Add(":NOMBRE", acudiente.PrimerNombre);
                comando.Parameters.Add(":APELLIDO", acudiente.PrimerApellido);
                comando.Parameters.Add(":PARENTESCO", acudiente.Parentesco);
                comando.Parameters.Add(":CORREO", acudiente.Correo);
                comando.Parameters.Add(":SEXO", acudiente.Sexo);
              
                comando.ExecuteNonQuery();

            }
        }
    }
}

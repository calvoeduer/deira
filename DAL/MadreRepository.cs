﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Entity;

namespace DAL
{
    public class MadreRepository
    {

        private OracleConnection conexion;
        List<Madre> madres;
        OracleDataReader Reader;

        public MadreRepository(OracleConnection conexionDB)
        {
            madres = new List<Madre>();
            conexion = conexionDB;

        }


        public void Guardar(Madre madre)
        {
            using (var comando = conexion.CreateCommand())
            {
                comando.CommandText = "INSERT INTO madre(NUMERODOCUMENTO, PRIMERNOMBRE, SEGUNDONOMBRE, PRIMERAPELLIDO, SEGUNDOAPELLIDO, FECHANACIMIENTO, SEXO, PUNTAJESISBEN, ESTADO) " +
                    " VALUES(:NUMERODOCUMENTO, :PRIMERNOMBRE, :SEGUNDONOMBRE, :PRIMERAPELLIDO, :SEGUNDOAPELLIDO, :FECHANACIMIENTO, :SEXO, :PUNTAJESISBEN, :ESTADO)";
                comando.Parameters.Add(":NUMERODOCUMENTO", madre.Identificacion);
                comando.Parameters.Add(":PRIMERNOMBRE", madre.PrimerNombre);
                comando.Parameters.Add(":SEGUNDONOMBRE", madre.SegundoNombre);
                comando.Parameters.Add(":PRIMERAPELLIDO", madre.PrimerApellido);
                comando.Parameters.Add(":SEGUNDOAPELLIDO", madre.SegundoApellido);
                comando.Parameters.Add(":FECHANACIMIENTO", madre.FechaNacimiento);
                comando.Parameters.Add(":SEXO", madre.Sexo);
                comando.Parameters.Add(":PUNTAJESISBEN", madre.PuntajeSisben);
                comando.Parameters.Add(":ESTADO", madre.EstadoMadre);



                comando.ExecuteNonQuery();

            }
        }
    }
}

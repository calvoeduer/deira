﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Entity;

namespace Proyecto_2
{
    public partial class FrmVacunacion : Form
    {
        public VacunaService vacunaService = new VacunaService();


        public FrmVacunacion()
        {
            InitializeComponent();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            FrmPrincipal principal_2 = new FrmPrincipal();
            principal_2.Show();
            this.Close();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnBuscarVacuna_Click(object sender, EventArgs e)
        {
            PnHistorialVacunas.Visible = true;
        }

        private void PctCerrar_Click(object sender, EventArgs e)
        {
            PnHistorialVacunas.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmConsultaVacunas frmConsultaVacunas = new FrmConsultaVacunas();
            frmConsultaVacunas.Show();
        }

        private void BtnGuardarVacuna_Click(object sender, EventArgs e)
        {

            try
            {
                Vacuna vacuna = new Vacuna();
                vacuna.IdVacuna = TxtVacunacion.Text;
                vacuna.EdadRecomendada = int.Parse(TxtEdadrecomendad.Text);
                vacuna.NombreVacuna = CmbVacuna.SelectedItem.ToString();
                string mensaje = vacunaService.Guardar(vacuna);
                MessageBox.Show(mensaje, "MensajeGuardarvacuna", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception)
            {

                MessageBox.Show("Error al ingresar datos", "MensajeGuardar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
           
            
        }

        private void FrmVacunacion_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Entity;

namespace Proyecto_2
{
    public partial class FrmConsultaVacunas : Form
    {
        VacunaService vacunaService = new VacunaService();
        
        public FrmConsultaVacunas()
        {
            InitializeComponent();
            TablaVacunas.DataSource = vacunaService.Consultar();
        }

        private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void PctCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnConsultarVacunas_Click(object sender, EventArgs e)
        {
            TablaVacunas.DataSource = null;
            TablaVacunas.DataSource = vacunaService.Consultar();
        }
    }
}

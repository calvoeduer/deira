﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_2
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        
        const string USER = "USER";
        const string PASS = "ADMIN";

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit(); 
        }

        private void TxtUsuario_Enter(object sender, EventArgs e)
        {
            if (TxtUsuario.Text == "USUARIO:")
            {
                TxtUsuario.Text = ""; 
            }
        }

        private void TxtUsuario_Leave(object sender, EventArgs e)
        {
            if (TxtUsuario.Text =="")
            {
                TxtUsuario.Text = "USUARIO:"; 
            }
        }

        private void TxtContrsaseña_Enter(object sender, EventArgs e)
        {
            if (TxtContrsaseña.Text == "CONTRASEÑA:")
            {
                TxtContrsaseña.Text = "";
                TxtContrsaseña.UseSystemPasswordChar = true;   // oculta los caracteres
            }
        }

        private void TxtContrsaseña_Leave(object sender, EventArgs e)
        {
            if (TxtContrsaseña.Text == "")
            {
                TxtContrsaseña.Text = "CONTRASEÑA:";
            }
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            if (TxtUsuario.Text.ToUpper().Equals(USER)&& TxtContrsaseña.Text.ToUpper().Equals(PASS))
            {
                FrmPrincipal principal_2 = new FrmPrincipal();
                this.Hide();
                principal_2.Show();
               
            }
            else
            {
                string M = "Usuario o Contraseña incorrcto "; 
                    MessageBox.Show(M, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
               
            }
        }

        private void TxtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtContrsaseña_TextChanged(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}

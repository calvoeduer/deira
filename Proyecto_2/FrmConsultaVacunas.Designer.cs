﻿namespace Proyecto_2
{
    partial class FrmConsultaVacunas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TablaVacunas = new System.Windows.Forms.DataGridView();
            this.BtnConsultarVacunas = new System.Windows.Forms.Button();
            this.PctCerrar = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TablaVacunas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctCerrar)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TablaVacunas
            // 
            this.TablaVacunas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TablaVacunas.Location = new System.Drawing.Point(76, 120);
            this.TablaVacunas.Name = "TablaVacunas";
            this.TablaVacunas.Size = new System.Drawing.Size(624, 287);
            this.TablaVacunas.TabIndex = 0;
            // 
            // BtnConsultarVacunas
            // 
            this.BtnConsultarVacunas.Location = new System.Drawing.Point(320, 440);
            this.BtnConsultarVacunas.Name = "BtnConsultarVacunas";
            this.BtnConsultarVacunas.Size = new System.Drawing.Size(143, 47);
            this.BtnConsultarVacunas.TabIndex = 5;
            this.BtnConsultarVacunas.Text = "Consultar";
            this.BtnConsultarVacunas.UseVisualStyleBackColor = true;
            this.BtnConsultarVacunas.Click += new System.EventHandler(this.BtnConsultarVacunas_Click);
            // 
            // PctCerrar
            // 
            this.PctCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PctCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(95)))), ((int)(((byte)(85)))));
            this.PctCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PctCerrar.Image = global::Proyecto_2.Properties.Resources.cerrar;
            this.PctCerrar.Location = new System.Drawing.Point(747, 11);
            this.PctCerrar.Name = "PctCerrar";
            this.PctCerrar.Size = new System.Drawing.Size(35, 35);
            this.PctCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctCerrar.TabIndex = 6;
            this.PctCerrar.TabStop = false;
            this.PctCerrar.Click += new System.EventHandler(this.PctCerrar_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(95)))), ((int)(((byte)(85)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.PctCerrar);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 56);
            this.panel1.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(266, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(269, 37);
            this.label3.TabIndex = 8;
            this.label3.Text = "Consultar Todos";
            // 
            // FrmConsultaVacunas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 518);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.BtnConsultarVacunas);
            this.Controls.Add(this.TablaVacunas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmConsultaVacunas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmConsultaVacunas";
            ((System.ComponentModel.ISupportInitialize)(this.TablaVacunas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctCerrar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView TablaVacunas;
        private System.Windows.Forms.Button BtnConsultarVacunas;
        private System.Windows.Forms.PictureBox PctCerrar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
    }
}
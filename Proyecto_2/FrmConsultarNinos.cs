﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_2
{
    class FrmConsultarNinos : Form
    {
        private Panel panel1;
        private DataGridView TBconsultarNinos;
        private Label label1;

        public FrmConsultarNinos()
        {
            InitializeComponent();
        }


        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.TBconsultarNinos = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TBconsultarNinos)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(95)))), ((int)(((byte)(85)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-55, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(985, 55);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(248, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(397, 39);
            this.label1.TabIndex = 1;
            this.label1.Text = "CONSULTA DE NIÑOS";
            // 
            // TBconsultarNinos
            // 
            this.TBconsultarNinos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TBconsultarNinos.Location = new System.Drawing.Point(93, 92);
            this.TBconsultarNinos.Name = "TBconsultarNinos";
            this.TBconsultarNinos.Size = new System.Drawing.Size(688, 331);
            this.TBconsultarNinos.TabIndex = 2;
            // 
            // FrmConsultarNinos
            // 
            this.ClientSize = new System.Drawing.Size(874, 515);
            this.Controls.Add(this.TBconsultarNinos);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmConsultarNinos";
            this.Load += new System.EventHandler(this.FrmConsultarNinos_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TBconsultarNinos)).EndInit();
            this.ResumeLayout(false);

        }

        private void FrmConsultarNinos_Load(object sender, EventArgs e)
        {

        }
    }
}

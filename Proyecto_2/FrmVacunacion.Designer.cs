﻿namespace Proyecto_2
{
    partial class FrmVacunacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.PnHistorialVacunas = new System.Windows.Forms.Panel();
            this.PctCerrar = new System.Windows.Forms.PictureBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EdadVacunacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CmbVacuna = new System.Windows.Forms.ComboBox();
            this.BtnBuscarVacuna = new System.Windows.Forms.Button();
            this.TxtVacunacion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.BtnGuardarVacuna = new System.Windows.Forms.Button();
            this.TxtEdadrecomendad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.PnHistorialVacunas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PctCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PnHistorialVacunas);
            this.panel1.Controls.Add(this.CmbVacuna);
            this.panel1.Controls.Add(this.BtnBuscarVacuna);
            this.panel1.Controls.Add(this.TxtVacunacion);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.BtnGuardarVacuna);
            this.panel1.Controls.Add(this.TxtEdadrecomendad);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(199, 79);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(622, 475);
            this.panel1.TabIndex = 4;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // PnHistorialVacunas
            // 
            this.PnHistorialVacunas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.PnHistorialVacunas.Controls.Add(this.PctCerrar);
            this.PnHistorialVacunas.Controls.Add(this.dataGridView1);
            this.PnHistorialVacunas.Location = new System.Drawing.Point(44, 301);
            this.PnHistorialVacunas.Name = "PnHistorialVacunas";
            this.PnHistorialVacunas.Size = new System.Drawing.Size(534, 171);
            this.PnHistorialVacunas.TabIndex = 20;
            this.PnHistorialVacunas.Visible = false;
            this.PnHistorialVacunas.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // PctCerrar
            // 
            this.PctCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PctCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.PctCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PctCerrar.Image = global::Proyecto_2.Properties.Resources.retroceso;
            this.PctCerrar.Location = new System.Drawing.Point(509, 3);
            this.PctCerrar.Name = "PctCerrar";
            this.PctCerrar.Size = new System.Drawing.Size(24, 18);
            this.PctCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctCerrar.TabIndex = 2;
            this.PctCerrar.TabStop = false;
            this.PctCerrar.Click += new System.EventHandler(this.PctCerrar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.Nombre,
            this.Fecha,
            this.EdadVacunacion});
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(507, 153);
            this.dataGridView1.TabIndex = 0;
            // 
            // Codigo
            // 
            this.Codigo.HeaderText = "Codigo";
            this.Codigo.Name = "Codigo";
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            // 
            // Fecha
            // 
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            // 
            // EdadVacunacion
            // 
            this.EdadVacunacion.HeaderText = "Edad Vacunacion";
            this.EdadVacunacion.Name = "EdadVacunacion";
            // 
            // CmbVacuna
            // 
            this.CmbVacuna.FormattingEnabled = true;
            this.CmbVacuna.Items.AddRange(new object[] {
            "Vacuna 1",
            "Vacuna 2",
            "Vacuna 3",
            "Vacuna 4",
            "Vacuna 5"});
            this.CmbVacuna.Location = new System.Drawing.Point(318, 57);
            this.CmbVacuna.Name = "CmbVacuna";
            this.CmbVacuna.Size = new System.Drawing.Size(151, 21);
            this.CmbVacuna.TabIndex = 19;
            // 
            // BtnBuscarVacuna
            // 
            this.BtnBuscarVacuna.Location = new System.Drawing.Point(245, 224);
            this.BtnBuscarVacuna.Name = "BtnBuscarVacuna";
            this.BtnBuscarVacuna.Size = new System.Drawing.Size(113, 42);
            this.BtnBuscarVacuna.TabIndex = 18;
            this.BtnBuscarVacuna.Text = "Buscar Vacuna";
            this.BtnBuscarVacuna.UseVisualStyleBackColor = true;
            this.BtnBuscarVacuna.Click += new System.EventHandler(this.BtnBuscarVacuna_Click);
            // 
            // TxtVacunacion
            // 
            this.TxtVacunacion.Location = new System.Drawing.Point(318, 16);
            this.TxtVacunacion.Name = "TxtVacunacion";
            this.TxtVacunacion.Size = new System.Drawing.Size(151, 20);
            this.TxtVacunacion.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(85, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Identificacion Beneficiario";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(403, 224);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(151, 42);
            this.button3.TabIndex = 15;
            this.button3.Text = "Consultar Usuarios Con Vacunas";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // BtnGuardarVacuna
            // 
            this.BtnGuardarVacuna.Location = new System.Drawing.Point(55, 224);
            this.BtnGuardarVacuna.Name = "BtnGuardarVacuna";
            this.BtnGuardarVacuna.Size = new System.Drawing.Size(113, 42);
            this.BtnGuardarVacuna.TabIndex = 13;
            this.BtnGuardarVacuna.Text = "Guardar Vacuna";
            this.BtnGuardarVacuna.UseVisualStyleBackColor = true;
            this.BtnGuardarVacuna.Click += new System.EventHandler(this.BtnGuardarVacuna_Click);
            // 
            // TxtEdadrecomendad
            // 
            this.TxtEdadrecomendad.Location = new System.Drawing.Point(318, 110);
            this.TxtEdadrecomendad.Name = "TxtEdadrecomendad";
            this.TxtEdadrecomendad.Size = new System.Drawing.Size(151, 20);
            this.TxtEdadrecomendad.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(116, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Edad recomendada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(139, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre Vacuna";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(95)))), ((int)(((byte)(85)))));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(-1, -4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1010, 55);
            this.panel2.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(414, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 37);
            this.label2.TabIndex = 0;
            this.label2.Text = "Vacunas";
            // 
            // FrmVacunacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1010, 578);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(250, 37);
            this.Name = "FrmVacunacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmVacunacion_5";
            this.Load += new System.EventHandler(this.FrmVacunacion_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PnHistorialVacunas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PctCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox TxtEdadrecomendad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtVacunacion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button BtnGuardarVacuna;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnBuscarVacuna;
        private System.Windows.Forms.Panel PnHistorialVacunas;
        private System.Windows.Forms.ComboBox CmbVacuna;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn EdadVacunacion;
        private System.Windows.Forms.PictureBox PctCerrar;
    }
}
﻿namespace Proyecto_2
{
    partial class FrmRegistrar_usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistrar_usuario));
            this.PnDatosBasicos = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.BtnSubirFosyga = new System.Windows.Forms.Button();
            this.CmbTipoBeneficiario = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPuntajeSisben = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.FechaNacimientoB = new System.Windows.Forms.DateTimePicker();
            this.CmbSexoB = new System.Windows.Forms.ComboBox();
            this.TxtPrimerNombreB = new System.Windows.Forms.TextBox();
            this.TxtSegundoNombre = new System.Windows.Forms.TextBox();
            this.TxtPrimerApellidoB = new System.Windows.Forms.TextBox();
            this.TxtSegundoApellidoB = new System.Windows.Forms.TextBox();
            this.TxtNumeroDocumentoB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PnAcudiente = new System.Windows.Forms.Panel();
            this.CmbParentescoA = new System.Windows.Forms.ComboBox();
            this.CmbEstadoMadre = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtCorreoElectronicoA = new System.Windows.Forms.TextBox();
            this.TxtApellidoA = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtNombreA = new System.Windows.Forms.TextBox();
            this.TxtNumeroDocumentoAcudiente = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnRegistrar = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtAltura = new System.Windows.Forms.TextBox();
            this.TxtPeso = new System.Windows.Forms.TextBox();
            this.CmbSexoA = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.PnDatosBasicos.SuspendLayout();
            this.PnAcudiente.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PnDatosBasicos
            // 
            resources.ApplyResources(this.PnDatosBasicos, "PnDatosBasicos");
            this.PnDatosBasicos.Controls.Add(this.button2);
            this.PnDatosBasicos.Controls.Add(this.BtnSubirFosyga);
            this.PnDatosBasicos.Controls.Add(this.CmbTipoBeneficiario);
            this.PnDatosBasicos.Controls.Add(this.label9);
            this.PnDatosBasicos.Controls.Add(this.TxtPuntajeSisben);
            this.PnDatosBasicos.Controls.Add(this.label8);
            this.PnDatosBasicos.Controls.Add(this.label7);
            this.PnDatosBasicos.Controls.Add(this.FechaNacimientoB);
            this.PnDatosBasicos.Controls.Add(this.CmbSexoB);
            this.PnDatosBasicos.Controls.Add(this.TxtPrimerNombreB);
            this.PnDatosBasicos.Controls.Add(this.TxtSegundoNombre);
            this.PnDatosBasicos.Controls.Add(this.TxtPrimerApellidoB);
            this.PnDatosBasicos.Controls.Add(this.TxtSegundoApellidoB);
            this.PnDatosBasicos.Controls.Add(this.TxtNumeroDocumentoB);
            this.PnDatosBasicos.Controls.Add(this.label6);
            this.PnDatosBasicos.Controls.Add(this.label5);
            this.PnDatosBasicos.Controls.Add(this.label4);
            this.PnDatosBasicos.Controls.Add(this.label3);
            this.PnDatosBasicos.Controls.Add(this.label2);
            this.PnDatosBasicos.Controls.Add(this.label1);
            this.PnDatosBasicos.Name = "PnDatosBasicos";
            this.PnDatosBasicos.Paint += new System.Windows.Forms.PaintEventHandler(this.PnDatosBasicos_Paint);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // BtnSubirFosyga
            // 
            resources.ApplyResources(this.BtnSubirFosyga, "BtnSubirFosyga");
            this.BtnSubirFosyga.Name = "BtnSubirFosyga";
            this.BtnSubirFosyga.UseVisualStyleBackColor = true;
            this.BtnSubirFosyga.Click += new System.EventHandler(this.BtnSubirFosyga_Click);
            // 
            // CmbTipoBeneficiario
            // 
            resources.ApplyResources(this.CmbTipoBeneficiario, "CmbTipoBeneficiario");
            this.CmbTipoBeneficiario.FormattingEnabled = true;
            this.CmbTipoBeneficiario.Items.AddRange(new object[] {
            resources.GetString("CmbTipoBeneficiario.Items"),
            resources.GetString("CmbTipoBeneficiario.Items1")});
            this.CmbTipoBeneficiario.Name = "CmbTipoBeneficiario";
            this.CmbTipoBeneficiario.SelectedIndexChanged += new System.EventHandler(this.CmbTipoBeneficiario_SelectedIndexChanged);
            this.CmbTipoBeneficiario.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Cmb_MouseClick);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // TxtPuntajeSisben
            // 
            resources.ApplyResources(this.TxtPuntajeSisben, "TxtPuntajeSisben");
            this.TxtPuntajeSisben.Name = "TxtPuntajeSisben";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // FechaNacimientoB
            // 
            resources.ApplyResources(this.FechaNacimientoB, "FechaNacimientoB");
            this.FechaNacimientoB.Name = "FechaNacimientoB";
            this.FechaNacimientoB.Value = new System.DateTime(2019, 11, 14, 19, 6, 36, 0);
            // 
            // CmbSexoB
            // 
            resources.ApplyResources(this.CmbSexoB, "CmbSexoB");
            this.CmbSexoB.FormattingEnabled = true;
            this.CmbSexoB.Items.AddRange(new object[] {
            resources.GetString("CmbSexoB.Items"),
            resources.GetString("CmbSexoB.Items1")});
            this.CmbSexoB.Name = "CmbSexoB";
            // 
            // TxtPrimerNombreB
            // 
            resources.ApplyResources(this.TxtPrimerNombreB, "TxtPrimerNombreB");
            this.TxtPrimerNombreB.Name = "TxtPrimerNombreB";
            // 
            // TxtSegundoNombre
            // 
            resources.ApplyResources(this.TxtSegundoNombre, "TxtSegundoNombre");
            this.TxtSegundoNombre.Name = "TxtSegundoNombre";
            // 
            // TxtPrimerApellidoB
            // 
            resources.ApplyResources(this.TxtPrimerApellidoB, "TxtPrimerApellidoB");
            this.TxtPrimerApellidoB.Name = "TxtPrimerApellidoB";
            // 
            // TxtSegundoApellidoB
            // 
            resources.ApplyResources(this.TxtSegundoApellidoB, "TxtSegundoApellidoB");
            this.TxtSegundoApellidoB.Name = "TxtSegundoApellidoB";
            // 
            // TxtNumeroDocumentoB
            // 
            resources.ApplyResources(this.TxtNumeroDocumentoB, "TxtNumeroDocumentoB");
            this.TxtNumeroDocumentoB.Name = "TxtNumeroDocumentoB";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // PnAcudiente
            // 
            this.PnAcudiente.Controls.Add(this.CmbSexoA);
            this.PnAcudiente.Controls.Add(this.label19);
            this.PnAcudiente.Controls.Add(this.CmbParentescoA);
            this.PnAcudiente.Controls.Add(this.CmbEstadoMadre);
            this.PnAcudiente.Controls.Add(this.label18);
            this.PnAcudiente.Controls.Add(this.label15);
            this.PnAcudiente.Controls.Add(this.TxtCorreoElectronicoA);
            this.PnAcudiente.Controls.Add(this.TxtApellidoA);
            this.PnAcudiente.Controls.Add(this.label13);
            this.PnAcudiente.Controls.Add(this.label14);
            this.PnAcudiente.Controls.Add(this.panel2);
            this.PnAcudiente.Controls.Add(this.TxtNombreA);
            this.PnAcudiente.Controls.Add(this.TxtNumeroDocumentoAcudiente);
            this.PnAcudiente.Controls.Add(this.label10);
            this.PnAcudiente.Controls.Add(this.label11);
            resources.ApplyResources(this.PnAcudiente, "PnAcudiente");
            this.PnAcudiente.Name = "PnAcudiente";
            this.PnAcudiente.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // CmbParentescoA
            // 
            resources.ApplyResources(this.CmbParentescoA, "CmbParentescoA");
            this.CmbParentescoA.FormattingEnabled = true;
            this.CmbParentescoA.Items.AddRange(new object[] {
            resources.GetString("CmbParentescoA.Items"),
            resources.GetString("CmbParentescoA.Items1"),
            resources.GetString("CmbParentescoA.Items2"),
            resources.GetString("CmbParentescoA.Items3"),
            resources.GetString("CmbParentescoA.Items4"),
            resources.GetString("CmbParentescoA.Items5")});
            this.CmbParentescoA.Name = "CmbParentescoA";
            // 
            // CmbEstadoMadre
            // 
            resources.ApplyResources(this.CmbEstadoMadre, "CmbEstadoMadre");
            this.CmbEstadoMadre.FormattingEnabled = true;
            this.CmbEstadoMadre.Items.AddRange(new object[] {
            resources.GetString("CmbEstadoMadre.Items"),
            resources.GetString("CmbEstadoMadre.Items1")});
            this.CmbEstadoMadre.Name = "CmbEstadoMadre";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // TxtCorreoElectronicoA
            // 
            resources.ApplyResources(this.TxtCorreoElectronicoA, "TxtCorreoElectronicoA");
            this.TxtCorreoElectronicoA.Name = "TxtCorreoElectronicoA";
            this.TxtCorreoElectronicoA.ReadOnly = true;
            // 
            // TxtApellidoA
            // 
            resources.ApplyResources(this.TxtApellidoA, "TxtApellidoA");
            this.TxtApellidoA.Name = "TxtApellidoA";
            this.TxtApellidoA.ReadOnly = true;
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(95)))), ((int)(((byte)(85)))));
            this.panel2.Controls.Add(this.label12);
            this.panel2.Name = "panel2";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Name = "label12";
            // 
            // TxtNombreA
            // 
            resources.ApplyResources(this.TxtNombreA, "TxtNombreA");
            this.TxtNombreA.Name = "TxtNombreA";
            this.TxtNombreA.ReadOnly = true;
            // 
            // TxtNumeroDocumentoAcudiente
            // 
            resources.ApplyResources(this.TxtNumeroDocumentoAcudiente, "TxtNumeroDocumentoAcudiente");
            this.TxtNumeroDocumentoAcudiente.Name = "TxtNumeroDocumentoAcudiente";
            this.TxtNumeroDocumentoAcudiente.ReadOnly = true;
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnRegistrar);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.TxtAltura);
            this.panel1.Controls.Add(this.TxtPeso);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // BtnRegistrar
            // 
            resources.ApplyResources(this.BtnRegistrar, "BtnRegistrar");
            this.BtnRegistrar.Name = "BtnRegistrar";
            this.BtnRegistrar.UseVisualStyleBackColor = true;
            this.BtnRegistrar.Click += new System.EventHandler(this.BtnRegistrar_Click);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // TxtAltura
            // 
            resources.ApplyResources(this.TxtAltura, "TxtAltura");
            this.TxtAltura.Name = "TxtAltura";
            // 
            // TxtPeso
            // 
            resources.ApplyResources(this.TxtPeso, "TxtPeso");
            this.TxtPeso.Name = "TxtPeso";
            // 
            // CmbSexoA
            // 
            resources.ApplyResources(this.CmbSexoA, "CmbSexoA");
            this.CmbSexoA.FormattingEnabled = true;
            this.CmbSexoA.Items.AddRange(new object[] {
            resources.GetString("CmbSexoA.Items"),
            resources.GetString("CmbSexoA.Items1")});
            this.CmbSexoA.Name = "CmbSexoA";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // FrmRegistrar_usuario
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PnAcudiente);
            this.Controls.Add(this.PnDatosBasicos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FrmRegistrar_usuario";
            this.Load += new System.EventHandler(this.Registrar_usuario_Load);
            this.PnDatosBasicos.ResumeLayout(false);
            this.PnDatosBasicos.PerformLayout();
            this.PnAcudiente.ResumeLayout(false);
            this.PnAcudiente.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PnDatosBasicos;
        private System.Windows.Forms.ComboBox CmbSexoB;
        private System.Windows.Forms.TextBox TxtPrimerNombreB;
        private System.Windows.Forms.TextBox TxtSegundoNombre;
        private System.Windows.Forms.TextBox TxtPrimerApellidoB;
        private System.Windows.Forms.TextBox TxtSegundoApellidoB;
        private System.Windows.Forms.TextBox TxtNumeroDocumentoB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker FechaNacimientoB;
        private System.Windows.Forms.ComboBox CmbTipoBeneficiario;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtPuntajeSisben;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel PnAcudiente;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TxtNombreA;
        private System.Windows.Forms.TextBox TxtNumeroDocumentoAcudiente;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TxtCorreoElectronicoA;
        private System.Windows.Forms.TextBox TxtApellidoA;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button BtnSubirFosyga;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TxtAltura;
        private System.Windows.Forms.TextBox TxtPeso;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox CmbEstadoMadre;
        private System.Windows.Forms.Button BtnRegistrar;
        private System.Windows.Forms.ComboBox CmbParentescoA;
        private System.Windows.Forms.ComboBox CmbSexoA;
        private System.Windows.Forms.Label label19;
    }
}
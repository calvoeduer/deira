﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_2
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void Principal_2_Load(object sender, EventArgs e)
        {
            
        }

        private void PctMenu_Click(object sender, EventArgs e)
        {
            if (PnlVertical.Width == 250)
            {
                PnlVertical.Width = 80;
            }
            else
            {
                PnlVertical.Width = 250; 
            }
        }

        private void PctCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit(); 
        }

        private void PctMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;

            PctMaximizar.Visible = false;
            if (PctMaximizar.Visible is false)
            {
                PctRestaurar.Visible = true;
            }
            
        }

        private void PctMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized; 
        }

        private void PctRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal; 
            PctRestaurar.Visible = false;
            PctMaximizar.Visible = true;
        }

        private void BtnAsistencia_Click(object sender, EventArgs e)
        {
            //ControlAsistencia_3 controlAsistencia_3 = new ControlAsistencia_3();
            //controlAsistencia_3.Show();
            //this.Close();

            AbrirFormularios(new FrmControlAsistencia());
          
        }


        private void AbrirFormularios(object formhijo)
        {
            if (this.PnlPrincipal.Controls.Count > 0)
                this.PnlPrincipal.Controls.RemoveAt(0);

                Form frm = formhijo as Form;
                frm.TopLevel = false;
                frm.Dock = DockStyle.Fill;
                this.PnlPrincipal.Controls.Add(frm);
                this.PnlPrincipal.Tag = frm;
                frm.Show();
    
        }

        private void BtnBeneficiario_Click(object sender, EventArgs e)
        {
            //UsuarioGestion_4 usuarioGestion_4 = new UsuarioGestion_4();
            //usuarioGestion_4.Show();
            //this.Close(); 

            AbrirFormularios(new FrmUsuarioGestion());
            
        }

        private void PnlPrincipal_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnVacunas_Click(object sender, EventArgs e)
        {
            AbrirFormularios(new FrmVacunacion());
        }

        private void FrmPrincipal_Resize(object sender, EventArgs e)
        {

        }

        private void HoraAndFecha_Tick(object sender, EventArgs e)
        {
            LblHora.Text = DateTime.Now.ToString("hh:mm tt");
            LblFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AbrirFormularios(new FrmComunicacion());
        }

        private void PnlVertical_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Mouse_Move(object sender, MouseEventArgs e)
        {
            if (PnlVertical.Width == 250)
            {
                PnlVertical.Width = 80;
            }
            else
            {
                PnlVertical.Width = 250;
            }
        }

        private void BtnConfiguracion_Click(object sender, EventArgs e)
        {
            AbrirFormularios(new FrmConfiguracion());
        }
    }
}

﻿using BLL;
using Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_2
{



    public partial class FrmRegistrar_usuario : Form
    {
        MadreService madreService = new MadreService();
        NinoService ninoService = new NinoService();
        AcudienteService acudienteService = new AcudienteService();

        public FrmRegistrar_usuario()
        {
            InitializeComponent();
            //ComboBox1.DropDownStyle = ComboBoxStyle.DropDownList
            CmbSexoB.DropDownStyle = ComboBoxStyle.DropDownList;
            CmbEstadoMadre.DropDownStyle = ComboBoxStyle.DropDownList;
            CmbTipoBeneficiario.DropDownStyle = ComboBoxStyle.DropDownList;
            CmbParentescoA.DropDownStyle = ComboBoxStyle.DropDownList;



        }




        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Registrar_usuario_Load(object sender, EventArgs e)
        {

        }

        private void Cmb_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void CmbTipoBeneficiario_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (CmbTipoBeneficiario.Text.Equals("NIÑO"))
            {
                TxtNumeroDocumentoAcudiente.ReadOnly = false;
                TxtNombreA.ReadOnly = false;
                TxtApellidoA.ReadOnly = false;
                CmbParentescoA.Enabled = true;
                TxtCorreoElectronicoA.ReadOnly = false;
                CmbEstadoMadre.Enabled = false;
                CmbEstadoMadre.Text = "";
                

            }
            else if (CmbTipoBeneficiario.Text.Equals("MADRE"))
            {
          

                TxtNumeroDocumentoAcudiente.ReadOnly = true;
                TxtNombreA.ReadOnly = true;
                TxtApellidoA.ReadOnly = true;
                CmbParentescoA.Enabled = false;
                TxtCorreoElectronicoA.ReadOnly = true;
                CmbEstadoMadre.Enabled = true;
            }


            if (CmbTipoBeneficiario.Text.Equals("MADRE"))
            {
                CmbSexoB.SelectedIndex = 1;
                CmbSexoB.Enabled = false;
            }
            else if (CmbTipoBeneficiario.Text.Equals("NIÑO"))
            {
                CmbSexoB.Enabled = true;
            }



        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

       

        private void BtnSubirFosyga_Click(object sender, EventArgs e)
        {
            
        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void PnDatosBasicos_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnRegistrar_Click(object sender, EventArgs e)
        {

          
            if (CmbTipoBeneficiario.Text.Equals("NIÑO"))
            {

                Nino nino = new Nino();
                nino.Identificacion = TxtNumeroDocumentoB.Text.Trim();
                nino.PrimerNombre = TxtPrimerNombreB.Text;
                nino.SegundoNombre = TxtSegundoNombre.Text;
                nino.PrimerApellido = TxtPrimerApellidoB.Text;
                nino.SegundoApellido = TxtSegundoApellidoB.Text;
                nino.FechaNacimiento = Convert.ToDateTime(FechaNacimientoB.Value.ToString("dd/MM/yyyy"));
                nino.Sexo = CmbSexoB.Text;
                nino.PuntajeSisben = TxtPuntajeSisben.Text;

                Acudiente acudiente = new Acudiente();
                acudiente.Identificacion = TxtNumeroDocumentoAcudiente.Text.Trim();
                acudiente.PrimerNombre = TxtNombreA.Text;
                acudiente.PrimerApellido = TxtApellidoA.Text;
                acudiente.Parentesco = CmbParentescoA.Text;
                acudiente.Correo = TxtCorreoElectronicoA.Text;
                acudiente.Sexo = CmbSexoA.Text;
                

                string mensajeacudiente = acudienteService.Guardar(acudiente);
                MessageBox.Show(mensajeacudiente, "MensajeGuardarAcudiente", MessageBoxButtons.OK, MessageBoxIcon.Information);



                string mensaje = ninoService.Guardar(nino);



                MessageBox.Show(mensaje, "MensajeGuardar", MessageBoxButtons.OK, MessageBoxIcon.Information);




            }
            else if (CmbTipoBeneficiario.Text.Equals("MADRE"))
            {
                Madre madre = new Madre();
                madre.Identificacion = TxtNumeroDocumentoB.Text;
                madre.PrimerNombre = TxtPrimerNombreB.Text;
                madre.SegundoNombre = TxtSegundoNombre.Text;
                madre.PrimerApellido = TxtPrimerApellidoB.Text;
                madre.SegundoApellido = TxtSegundoApellidoB.Text;
                madre.FechaNacimiento = Convert.ToDateTime(FechaNacimientoB.Value.ToString("dd/MM/yyyy"));
                madre.Sexo = CmbSexoB.Text;
                madre.PuntajeSisben = TxtPuntajeSisben.Text;
                madre.EstadoMadre = CmbEstadoMadre.SelectedItem.ToString();


                string mensaje = madreService.Guardar(madre);


                MessageBox.Show(mensaje, "MensajeGuardar", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }

            if (CmbTipoBeneficiario.Text.Equals("NIÑO"))
            {

            }


        }
    }
}

﻿namespace Proyecto_2
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PnlVertical = new System.Windows.Forms.Panel();
            this.LblFecha = new System.Windows.Forms.Label();
            this.LblHora = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.BtnConfiguracion = new System.Windows.Forms.Button();
            this.BtnVacunas = new System.Windows.Forms.Button();
            this.BtnBeneficiario = new System.Windows.Forms.Button();
            this.BtnAsistencia = new System.Windows.Forms.Button();
            this.PctLogo = new System.Windows.Forms.PictureBox();
            this.PnlHorizontal = new System.Windows.Forms.Panel();
            this.PctMaximizar = new System.Windows.Forms.PictureBox();
            this.PctRestaurar = new System.Windows.Forms.PictureBox();
            this.PctMinimizar = new System.Windows.Forms.PictureBox();
            this.PctCerrar = new System.Windows.Forms.PictureBox();
            this.PctMenu = new System.Windows.Forms.PictureBox();
            this.PnlPrincipal = new System.Windows.Forms.Panel();
            this.HoraAndFecha = new System.Windows.Forms.Timer(this.components);
            this.PnlVertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PctLogo)).BeginInit();
            this.PnlHorizontal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PctMaximizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctRestaurar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlVertical
            // 
            this.PnlVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(140)))), ((int)(((byte)(57)))));
            this.PnlVertical.Controls.Add(this.LblFecha);
            this.PnlVertical.Controls.Add(this.LblHora);
            this.PnlVertical.Controls.Add(this.button1);
            this.PnlVertical.Controls.Add(this.BtnConfiguracion);
            this.PnlVertical.Controls.Add(this.BtnVacunas);
            this.PnlVertical.Controls.Add(this.BtnBeneficiario);
            this.PnlVertical.Controls.Add(this.BtnAsistencia);
            this.PnlVertical.Controls.Add(this.PctLogo);
            this.PnlVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.PnlVertical.Location = new System.Drawing.Point(0, 0);
            this.PnlVertical.Name = "PnlVertical";
            this.PnlVertical.Size = new System.Drawing.Size(250, 617);
            this.PnlVertical.TabIndex = 0;
            this.PnlVertical.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlVertical_Paint);
            // 
            // LblFecha
            // 
            this.LblFecha.AutoSize = true;
            this.LblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFecha.ForeColor = System.Drawing.Color.White;
            this.LblFecha.Location = new System.Drawing.Point(41, 556);
            this.LblFecha.Name = "LblFecha";
            this.LblFecha.Size = new System.Drawing.Size(0, 22);
            this.LblFecha.TabIndex = 7;
            // 
            // LblHora
            // 
            this.LblHora.AutoSize = true;
            this.LblHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblHora.ForeColor = System.Drawing.Color.White;
            this.LblHora.Location = new System.Drawing.Point(20, 506);
            this.LblHora.Name = "LblHora";
            this.LblHora.Size = new System.Drawing.Size(0, 37);
            this.LblHora.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Window;
            this.button1.Image = global::Proyecto_2.Properties.Resources.mail;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(3, 305);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(232, 39);
            this.button1.TabIndex = 6;
            this.button1.Text = "Comunicación";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnConfiguracion
            // 
            this.BtnConfiguracion.FlatAppearance.BorderSize = 0;
            this.BtnConfiguracion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.BtnConfiguracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnConfiguracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnConfiguracion.ForeColor = System.Drawing.SystemColors.Window;
            this.BtnConfiguracion.Image = global::Proyecto_2.Properties.Resources.configuracion;
            this.BtnConfiguracion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnConfiguracion.Location = new System.Drawing.Point(3, 366);
            this.BtnConfiguracion.Name = "BtnConfiguracion";
            this.BtnConfiguracion.Size = new System.Drawing.Size(232, 39);
            this.BtnConfiguracion.TabIndex = 5;
            this.BtnConfiguracion.Text = "Configuracion";
            this.BtnConfiguracion.UseVisualStyleBackColor = true;
            this.BtnConfiguracion.Click += new System.EventHandler(this.BtnConfiguracion_Click);
            // 
            // BtnVacunas
            // 
            this.BtnVacunas.FlatAppearance.BorderSize = 0;
            this.BtnVacunas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.BtnVacunas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnVacunas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVacunas.ForeColor = System.Drawing.SystemColors.Window;
            this.BtnVacunas.Image = global::Proyecto_2.Properties.Resources.vacuna;
            this.BtnVacunas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnVacunas.Location = new System.Drawing.Point(9, 227);
            this.BtnVacunas.Name = "BtnVacunas";
            this.BtnVacunas.Size = new System.Drawing.Size(232, 56);
            this.BtnVacunas.TabIndex = 4;
            this.BtnVacunas.Text = "Vacunas";
            this.BtnVacunas.UseVisualStyleBackColor = true;
            this.BtnVacunas.Click += new System.EventHandler(this.BtnVacunas_Click);
            // 
            // BtnBeneficiario
            // 
            this.BtnBeneficiario.FlatAppearance.BorderSize = 0;
            this.BtnBeneficiario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.BtnBeneficiario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBeneficiario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBeneficiario.ForeColor = System.Drawing.SystemColors.Window;
            this.BtnBeneficiario.Image = global::Proyecto_2.Properties.Resources.bene;
            this.BtnBeneficiario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnBeneficiario.Location = new System.Drawing.Point(9, 166);
            this.BtnBeneficiario.Name = "BtnBeneficiario";
            this.BtnBeneficiario.Size = new System.Drawing.Size(232, 39);
            this.BtnBeneficiario.TabIndex = 3;
            this.BtnBeneficiario.Text = "Beneficiario";
            this.BtnBeneficiario.UseVisualStyleBackColor = true;
            this.BtnBeneficiario.Click += new System.EventHandler(this.BtnBeneficiario_Click);
            // 
            // BtnAsistencia
            // 
            this.BtnAsistencia.FlatAppearance.BorderSize = 0;
            this.BtnAsistencia.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.BtnAsistencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAsistencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAsistencia.ForeColor = System.Drawing.SystemColors.Window;
            this.BtnAsistencia.Image = global::Proyecto_2.Properties.Resources.asistencia;
            this.BtnAsistencia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnAsistencia.Location = new System.Drawing.Point(9, 105);
            this.BtnAsistencia.Name = "BtnAsistencia";
            this.BtnAsistencia.Size = new System.Drawing.Size(232, 39);
            this.BtnAsistencia.TabIndex = 1;
            this.BtnAsistencia.Text = "Asistencia";
            this.BtnAsistencia.UseVisualStyleBackColor = true;
            this.BtnAsistencia.Click += new System.EventHandler(this.BtnAsistencia_Click);
            // 
            // PctLogo
            // 
            this.PctLogo.Image = global::Proyecto_2.Properties.Resources.logo_Completo;
            this.PctLogo.Location = new System.Drawing.Point(3, 12);
            this.PctLogo.Name = "PctLogo";
            this.PctLogo.Size = new System.Drawing.Size(241, 88);
            this.PctLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctLogo.TabIndex = 2;
            this.PctLogo.TabStop = false;
            // 
            // PnlHorizontal
            // 
            this.PnlHorizontal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(140)))), ((int)(((byte)(57)))));
            this.PnlHorizontal.Controls.Add(this.PctMaximizar);
            this.PnlHorizontal.Controls.Add(this.PctRestaurar);
            this.PnlHorizontal.Controls.Add(this.PctMinimizar);
            this.PnlHorizontal.Controls.Add(this.PctCerrar);
            this.PnlHorizontal.Controls.Add(this.PctMenu);
            this.PnlHorizontal.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlHorizontal.Location = new System.Drawing.Point(250, 0);
            this.PnlHorizontal.Name = "PnlHorizontal";
            this.PnlHorizontal.Size = new System.Drawing.Size(1010, 42);
            this.PnlHorizontal.TabIndex = 0;
            // 
            // PctMaximizar
            // 
            this.PctMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PctMaximizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(140)))), ((int)(((byte)(57)))));
            this.PctMaximizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PctMaximizar.Image = global::Proyecto_2.Properties.Resources.maxi;
            this.PctMaximizar.Location = new System.Drawing.Point(936, 7);
            this.PctMaximizar.Name = "PctMaximizar";
            this.PctMaximizar.Size = new System.Drawing.Size(25, 25);
            this.PctMaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctMaximizar.TabIndex = 2;
            this.PctMaximizar.TabStop = false;
            this.PctMaximizar.Click += new System.EventHandler(this.PctMaximizar_Click);
            // 
            // PctRestaurar
            // 
            this.PctRestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PctRestaurar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(140)))), ((int)(((byte)(57)))));
            this.PctRestaurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PctRestaurar.Image = global::Proyecto_2.Properties.Resources.res;
            this.PctRestaurar.Location = new System.Drawing.Point(936, 6);
            this.PctRestaurar.Name = "PctRestaurar";
            this.PctRestaurar.Size = new System.Drawing.Size(25, 25);
            this.PctRestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctRestaurar.TabIndex = 4;
            this.PctRestaurar.TabStop = false;
            this.PctRestaurar.Visible = false;
            this.PctRestaurar.Click += new System.EventHandler(this.PctRestaurar_Click);
            // 
            // PctMinimizar
            // 
            this.PctMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PctMinimizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(140)))), ((int)(((byte)(57)))));
            this.PctMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PctMinimizar.Image = global::Proyecto_2.Properties.Resources.minimazar;
            this.PctMinimizar.Location = new System.Drawing.Point(900, 6);
            this.PctMinimizar.Name = "PctMinimizar";
            this.PctMinimizar.Size = new System.Drawing.Size(25, 25);
            this.PctMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctMinimizar.TabIndex = 3;
            this.PctMinimizar.TabStop = false;
            this.PctMinimizar.Click += new System.EventHandler(this.PctMinimizar_Click);
            // 
            // PctCerrar
            // 
            this.PctCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PctCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(140)))), ((int)(((byte)(57)))));
            this.PctCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PctCerrar.Image = global::Proyecto_2.Properties.Resources.cerrar;
            this.PctCerrar.Location = new System.Drawing.Point(972, 6);
            this.PctCerrar.Name = "PctCerrar";
            this.PctCerrar.Size = new System.Drawing.Size(25, 25);
            this.PctCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctCerrar.TabIndex = 1;
            this.PctCerrar.TabStop = false;
            this.PctCerrar.Click += new System.EventHandler(this.PctCerrar_Click);
            // 
            // PctMenu
            // 
            this.PctMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(191)))), ((int)(((byte)(68)))));
            this.PctMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PctMenu.Enabled = false;
            this.PctMenu.Image = global::Proyecto_2.Properties.Resources.menu2;
            this.PctMenu.Location = new System.Drawing.Point(6, 4);
            this.PctMenu.Name = "PctMenu";
            this.PctMenu.Size = new System.Drawing.Size(41, 30);
            this.PctMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PctMenu.TabIndex = 0;
            this.PctMenu.TabStop = false;
            this.PctMenu.Click += new System.EventHandler(this.PctMenu_Click);
            this.PctMenu.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Mouse_Move);
            // 
            // PnlPrincipal
            // 
            this.PnlPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PnlPrincipal.AutoSize = true;
            this.PnlPrincipal.Location = new System.Drawing.Point(250, 37);
            this.PnlPrincipal.Name = "PnlPrincipal";
            this.PnlPrincipal.Size = new System.Drawing.Size(1010, 578);
            this.PnlPrincipal.TabIndex = 1;
            this.PnlPrincipal.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlPrincipal_Paint);
            // 
            // HoraAndFecha
            // 
            this.HoraAndFecha.Enabled = true;
            this.HoraAndFecha.Tick += new System.EventHandler(this.HoraAndFecha_Tick);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1260, 617);
            this.Controls.Add(this.PnlPrincipal);
            this.Controls.Add(this.PnlHorizontal);
            this.Controls.Add(this.PnlVertical);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal_2";
            this.Load += new System.EventHandler(this.Principal_2_Load);
            this.Resize += new System.EventHandler(this.FrmPrincipal_Resize);
            this.PnlVertical.ResumeLayout(false);
            this.PnlVertical.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PctLogo)).EndInit();
            this.PnlHorizontal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PctMaximizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctRestaurar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PctMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PnlVertical;
        private System.Windows.Forms.PictureBox PctLogo;
        private System.Windows.Forms.Panel PnlHorizontal;
        private System.Windows.Forms.PictureBox PctMinimizar;
        private System.Windows.Forms.PictureBox PctMaximizar;
        private System.Windows.Forms.PictureBox PctCerrar;
        private System.Windows.Forms.PictureBox PctRestaurar;
        private System.Windows.Forms.Button BtnAsistencia;
        private System.Windows.Forms.Button BtnConfiguracion;
        private System.Windows.Forms.Button BtnVacunas;
        private System.Windows.Forms.Button BtnBeneficiario;
        private System.Windows.Forms.Panel PnlPrincipal;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox PctMenu;
        private System.Windows.Forms.Label LblFecha;
        private System.Windows.Forms.Label LblHora;
        private System.Windows.Forms.Timer HoraAndFecha;
    }
}